# State of the Map Africa 2023 Website

Repository for State of the Map Afirca 2023 website.

Want to contribute!

- See the [issues](https://gitlab.com/osmafrica/sotm-africa-2023/-/issues) for all discussions and tasks.

## Getting Started with Development

First, Fork and Clone to your local machine.

This site uses [Jekyll](https://jekyllrb.com/). To get started on GNU/Linux, Unix, or macOS, you must meet the following requirements:

- Ruby 2.2.5 or above
- RubyGems
- GCC and Make

Install [bundler](https://bundler.io/).

```
gem install bundler
```

Next, use bundler to install the build dependencies:

```
bundle install
```

Then, start Jekyll with bundler (this resolves any dependency issues you may have):

```
bundle exec jekyll serve -- --livereload
```

Finally, navigate to http://127.0.0.1:4000/ and you should see the new website!
